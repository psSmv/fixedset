#include <vector>
#include <cstdlib>
#include <iostream>

using std::srand;
using std::rand;
using std::vector;

// Достаточно большое простое число
const unsigned PRIME_NUMBER = 2147483647;

// Максимальное значение модуля аргументов хэш функции
const unsigned MAX_ABS_VALUE = 1000000000;

// Величина сдвига, прибавляя которую к аргументам хэш функции
// мы получим неотрицательные числа.
const unsigned SHIFT_KEY = MAX_ABS_VALUE;


// Хэш функция из универсального множества
class HashFunction {
    // linearCoef - случайное число в диапазоне [1; PrimeNumber - 1]
    // constCoef - случайное число в диапазоне [0; PrimeNumber - 1]
    unsigned linearCoef, constCoef;
    
public:
    HashFunction() {};
    
    // генерирует хэш-функцию
    static HashFunction generate() {
        HashFunction answer;
        answer.linearCoef = (rand() % (PRIME_NUMBER - 1)) + 1;
        answer.constCoef = rand() % PRIME_NUMBER;
        return answer;
    }
    
    // Вычисление возвращаемого значения
    // h(x) = (linearCoef * (x + SHIFT_KEY) + constCoef) % PRIME_NUMBER;
    unsigned operator () (long long value) const {
        value += SHIFT_KEY;
        long long result = (linearCoef * value + constCoef) % PRIME_NUMBER;
        return static_cast<unsigned>(result);
    };
};

// Идеальный хэш
class FixedSet {
    // Ребро графа
    struct Edge {
        unsigned destination; // куда ведет
        unsigned weight; // вес
        
        Edge(unsigned newWeight, unsigned newDestination) {
            weight = newWeight;
            destination = newDestination;
        }
    };
    // Вершина графа
    struct Node {
        vector<Edge> edges; // ребра
        unsigned weight; // вес
    };
    
    // Граф хранится как вектор вершин
    vector<Node> nodes;
    
    // Используемые для построения графа хэш-функции
    HashFunction first, second;
    
    // Число ребер
    unsigned numberOfEdges;
    
    // Во сколько раз вершин в графе больше чем ребер
    static const unsigned NODES_ON_EDGE = 3;
    
    // Вектор чисел, которыми проинициализирован идеальный хэш
    vector<int> initialData;
    
    // Создает ребра в графе
    // возвращает false если мы пытаемся создать петлю
    // и true иначе
    bool fillEdges(const vector<int>& numbers) {
        for (unsigned ii = 0; ii < numbers.size(); ++ii) {
            
            if ((first(numbers[ii]) % nodes.size()) == (second(numbers[ii]) % nodes.size())) {
                return false;
            }
            
            nodes[first(numbers[ii]) % nodes.size()].edges.
            push_back(Edge(ii, second(numbers[ii]) % nodes.size()));
            
            nodes[second(numbers[ii]) % nodes.size()].edges.
            push_back(Edge(ii, first(numbers[ii]) % nodes.size()));
        }
        return true;
    }
    
    // рекурентная процедура обхода графа
    // (в теории названа ПОСЕТИТЬ)
    // возвращает false если в процессе работы возникает ошибка
    // (в построенном графе есть цикл, в котором нельзя правильным образом расставить веса)
    // и true в иначе
    bool visitNode(unsigned startNode, unsigned value, vector<bool>& isVisited) {
        if (isVisited[startNode]) {
            if (nodes[startNode].weight != value) {
                return false;
            }
        } else {
            isVisited[startNode] = true;
            nodes[startNode].weight = value;
            for (unsigned ii = 0; ii < nodes[startNode].edges.size(); ++ii) {
                bool success;
                if (nodes[startNode].edges[ii].weight > value) {
                    unsigned nextStartNode = nodes[startNode].edges[ii].destination;
                    unsigned nextValue = (nodes[startNode].edges[ii].weight
                                          - value) % numberOfEdges;
                    
                    success = visitNode(nextStartNode, nextValue, isVisited);
                } else {
                    unsigned nextStartNode = nodes[startNode].edges[ii].destination;
                    unsigned nextValue = (nodes[startNode].edges[ii].weight
                                          - value + numberOfEdges) % numberOfEdges;
                    
                    success = visitNode(nextStartNode, nextValue, isVisited);
                }
                if (!success) {
                    return false;
                }
            }
        }
        return true;
    };
    
public:
    FixedSet() {
        first = HashFunction::generate();
        second = HashFunction::generate();
    };
    
    // Инициализирует идеальный хэш
    void Initialize(const vector<int>& numbers) {
        numberOfEdges = numbers.size();
        bool made = false;
        while (!made) {
            made = true;
            first = HashFunction::generate();
            second = HashFunction::generate();
            nodes = vector<Node>(NODES_ON_EDGE * numberOfEdges);
            vector<bool> isVisited(NODES_ON_EDGE * numberOfEdges, false);
            if (!fillEdges(numbers)) {
                made = false;
            };
            if (made) {
                for (unsigned i = 0; i < nodes.size(); ++i) {
                    if (!isVisited[i]) {
                        if (!visitNode(i, 0, isVisited)) {
                            made = false;
                            break;
                        }
                    }
                }
            }
        }
        initialData = numbers;
    };
    
    // Отвечает, содержится ли число в исходном множестве
    bool Contains (int number) const {
        return (number == initialData[
                                      (nodes[first(number) % nodes.size()].weight +
                                       nodes[second(number) % nodes.size()].weight)
                                      % numberOfEdges]);
    };
};

// Отвечает на запросы используя идеальный хэш
vector<bool> answerRequests(const vector<int>& numbers, const vector<int>& requests) {
    vector<bool> answers;
    FixedSet set;
    set.Initialize(numbers);
    for (int i = 0; i < requests.size(); ++i) {
        answers.push_back(set.Contains(requests[i]));
    }
    return answers;
};

// Считывает ввод
vector<int> scanInput(std::istream& inputStream = std::cin) {
    unsigned inputSize;
    vector<int> output;
    inputStream >> inputSize;
    for (unsigned i = 0; i < inputSize; ++i) {
        int newNumber;
        inputStream >> newNumber;
        output.push_back(newNumber);
    }
    return output;
};

// Печатает вывод
void printAnswers(const vector<bool>& answers, std::ostream& outputStream = std::cout) {
    for (int i = 0; i < answers.size(); ++i) {
        if (answers[i]) {
            outputStream << "Yes\n";
        } else {
            outputStream << "No\n";
        }
    }
};

int main() {
    
    std::ios_base::sync_with_stdio(false);
    std::srand(42);
    
    vector<int> numbers = scanInput();
    
    vector<int> requests = scanInput();
    
    vector<bool> answers = answerRequests(numbers, requests);
    
    printAnswers(answers);
    
    return 0;
}
